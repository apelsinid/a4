
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Quaternions. Basic operations. */
public class Quaternion {
   private static final double THRESHOLD = 0.000001;
   private double a;
   private double i;
   private double j;
   private double k;
   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      this.a = a;
      this.i = b;
      this.j = c;
      this.k = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return a;
   }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() {
      return i;
   }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
      return j;
   }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
      return k;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      // http://home.apache.org/~luc/commons-math-3.6-RC2-site/jacoco/org.apache.commons.math3.complex/Quaternion.java.html -> toString
      StringBuilder output = new StringBuilder();
      output.append(a)
              .append(("+" + i + "i").replace("+-", "-"))
              .append(("+" + j + "j").replace("+-", "-"))
              .append(("+" + k + "k").replace("+-", "-"));

      return output.toString();
   }

   /** Conversion from the string to the quaternion. 
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
      if (s.length() == 0 || s.trim().length() == 0) {
         throw new IllegalArgumentException(String.format("Cannot parse quaternion from empty string %s", s));
      }
      // https://enos.itcollege.ee/~jpoial/algorithms/examples/ -> Qregex.java
      System.out.println(s);
      String example = s;
      String orop = "|";
      String seq = "\\d+";
      String deli = "\\.";
      String fixp = "(" + seq +
              orop + seq + deli +
              orop + deli + seq +
              orop + seq + deli + seq + ")";
      String floa = "(" + fixp +
              orop + fixp + "[eE]" + "[+-]?"+ seq + ")";
      String doub = "(" +  floa +
              orop + floa + "[dD]" + ")";
      String real = "(" + doub +
              orop + "[+-]" + doub + ")";
      String imag = "(" + "[+-]" + doub + "[iI]" +
              orop + "[+-]?" + "[iI]" + ")";
      String jmag = "(" + "[+-]" + doub + "[jJ]" +
              orop + "[+-]?" + "[jJ]" + ")";
      String kmag = "(" + "[+-]" + doub + "[kK]" +
              orop + "[+-]?" + "[kK]" + ")";
      String expr = "(" + real +
              orop + real + imag +
              orop + real + imag + jmag +
              orop + real + imag + jmag + kmag +
              orop + real + imag + kmag +
              orop + real + jmag +
              orop + real + jmag + kmag +
              orop + real + kmag +
              orop + imag +
              orop + imag + jmag +
              orop + imag + jmag + kmag +
              orop + imag + kmag +
              orop + jmag +
              orop + jmag + kmag +
              orop + kmag + ")";
      String pattern = "^" + expr + "$";
      // System.out.println (pattern);
      Pattern r = Pattern.compile(pattern);
      // System.out.println(r);
      Matcher m = r.matcher(example);
      if (m.find()) {
         System.out.println("Found value: " + m.group(0) );
         // Groups selected with the help of https://regex101.com/
         String a = m.group(71).replaceAll("[+ijk]","");
         String i = m.group(86).replaceAll("[+ijk]","");
         String j = m.group(94).replaceAll("[+ijk]","");
         String k = m.group(102).replaceAll("[+ijk]","");
         return new Quaternion(
                 Double.parseDouble(a),
                 Double.parseDouble(i),
                 Double.parseDouble(j),
                 Double.parseDouble(k));
      }
      // System.out.println("NO MATCH");
      throw new IllegalArgumentException(String.format("Cannot parse quaternion from string %s", s));
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      Quaternion clone = new Quaternion(a, i, j, k);
      if (!equals(clone)) {
         throw new CloneNotSupportedException();
      }
      return clone;
   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      return Math.abs(a) < THRESHOLD &&
              Math.abs(i) < THRESHOLD &&
              Math.abs(j) < THRESHOLD &&
              Math.abs(k) < THRESHOLD;
   }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      // http://home.apache.org/~luc/commons-math-3.6-RC2-site/jacoco/org.apache.commons.math3.complex/Quaternion.java.html
      return new Quaternion(a, -i, -j, -k);
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      return new Quaternion(-a, -i, -j, -k);
   }

   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      return new Quaternion(a + q.getRpart(),
              i + q.getIpart(),
              j + q.getJpart(),
              k + q.getKpart());
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      // http://home.apache.org/~luc/commons-math-3.6-RC2-site/jacoco/org.apache.commons.math3.complex/Quaternion.java.html
      // Components of the first quaternion.
      final double q1a = a;
      final double q1b = i;
      final double q1c = j;
      final double q1d = k;

      // Components of the second quaternion.
      final double q2a = q.getRpart();
      final double q2b = q.getIpart();
      final double q2c = q.getJpart();
      final double q2d = q.getKpart();

      // Components of the product.
      final double w = q1a * q2a - q1b * q2b - q1c * q2c - q1d * q2d;
      final double x = q1a * q2b + q1b * q2a + q1c * q2d - q1d * q2c;
      final double y = q1a * q2c - q1b * q2d + q1c * q2a + q1d * q2b;
      final double z = q1a * q2d + q1b * q2c - q1c * q2b + q1d * q2a;

      return new Quaternion(w, x, y, z);
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      return new Quaternion(a * r, i * r, j * r, k * r);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
      // http://home.apache.org/~luc/commons-math-3.6-RC2-site/jacoco/org.apache.commons.math3.complex/Quaternion.java.html
      final double squareNorm = a * a + i * i + j * j + k * k;
      if (Math.abs(squareNorm) < THRESHOLD) {
         throw new RuntimeException(String.format("Division with zero: ", squareNorm));
      }
      return new Quaternion(a / squareNorm,
              -i / squareNorm,
              -j / squareNorm,
              -k / squareNorm);
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      return new Quaternion(
              a - q.getRpart(),
              i - q.getIpart(),
              j - q.getJpart(),
              k - q.getKpart()
      );
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      if (q.isZero()) {
         throw new RuntimeException(String.format("Division with zero ", q));
      }
      return times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
      if (q.isZero()) {
         throw new RuntimeException(String.format("Division with zero ", q));
      }
      return q.inverse().times(this);
   }
   
   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
      // http://home.apache.org/~luc/commons-math-3.6-RC2-site/jacoco/org.apache.commons.math3.complex/Quaternion.java.html
      if (this == qo) {
         return true;
      }
      if (qo instanceof Quaternion) {
         final Quaternion q = (Quaternion) qo;
         return Math.abs(a - q.getRpart()) < THRESHOLD &&
                 Math.abs(i - q.getIpart()) < THRESHOLD &&
                 Math.abs(j - q.getJpart()) < THRESHOLD &&
                 Math.abs(k - q.getKpart()) < THRESHOLD;
      }

      return false;
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
      Quaternion thisTimesOtherConjugate = times(q.conjugate());
      Quaternion otherTimesThisConjugate = q.times(conjugate());
      Quaternion sumOfMultiplications = thisTimesOtherConjugate.plus(otherTimesThisConjugate);
      return sumOfMultiplications.times(0.5);
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      // tostring + hashcode
      // System.out.println(toString().hashCode());
      return toString().hashCode();
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      final double squareNorm = a * a + i * i + j * j + k * k;
      return Math.sqrt(squareNorm);
   }

   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
         valueOf ("-1-1i+7j+13k");
//      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
//      if (arg.length > 0)
//         arv1 = valueOf (arg[0]);
//
//      System.out.println ("first: " + arv1.toString());
//      System.out.println ("real: " + arv1.getRpart());
//      System.out.println ("imagi: " + arv1.getIpart());
//      System.out.println ("imagj: " + arv1.getJpart());
//      System.out.println ("imagk: " + arv1.getKpart());
//      System.out.println ("isZero: " + arv1.isZero());
//      System.out.println ("conjugate: " + arv1.conjugate());
//      System.out.println ("opposite: " + arv1.opposite());
//      System.out.println ("hashCode: " + arv1.hashCode());
//      Quaternion res = null;
//      try {
//         res = (Quaternion)arv1.clone();
//      } catch (CloneNotSupportedException e) {};
//      System.out.println ("clone equals to original: " + res.equals (arv1));
//      System.out.println ("clone is not the same object: " + (res!=arv1));
//      System.out.println ("hashCode: " + res.hashCode());
//      res = valueOf (arv1.toString());
//      System.out.println ("string conversion equals to original: "
//         + res.equals (arv1));
//      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
//      if (arg.length > 1)
//         arv2 = valueOf (arg[1]);
//      System.out.println ("second: " + arv2.toString());
//      System.out.println ("hashCode: " + arv2.hashCode());
//      System.out.println ("equals: " + arv1.equals (arv2));
//      res = arv1.plus (arv2);
//      System.out.println ("plus: " + res);
//      System.out.println ("times: " + arv1.times (arv2));
//      System.out.println ("minus: " + arv1.minus (arv2));
//      double mm = arv1.norm();
//      System.out.println ("norm: " + mm);
//      System.out.println ("inverse: " + arv1.inverse());
//      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
//      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
//      System.out.println ("dotMult: " + arv1.dotMult (arv2));
   }
}
// end of file
